#ifndef XMLCREATOR_H
#define XMLCREATOR_H

#include <mxml.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "variables.h"

int generate_config_XML(struct tuple * tuples, int size);
int save_config_XML(mxml_node_t * tree);

#endif
