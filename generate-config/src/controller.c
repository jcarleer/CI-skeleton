#include "controller.h"
#include "xmlcreator.h"
#include "create_githook.h"

static struct option long_options[] = {
    {"apitoken",        required_argument, 0, 'a'},
    {"branch",          required_argument, 0, 'b'},
    {"credentialsid",   required_argument, 0, 'c'},
    {"jenkinsfilename", required_argument, 0, 'f'},
    {"giturl",          required_argument, 0, 'g'},
    {"help",            no_argument      , 0, 'h'},
    {"pipeline",        required_argument, 0, 'i'},
    {"jenkins",         required_argument, 0, 'j'},
    {"list",            no_argument      , 0, 'l'},
    {"mattermost",      required_argument, 0, 'm'},
    {"mattermost_hook", required_argument, 0, 'o'},
    {"pipe",            required_argument, 0, 'p'},
    {"periodictimer",   required_argument, 0, 'r'},
    {"token",           required_argument, 0, 't'},
    {0, 0, 0, 0}
};

int get_help()
{
    printf("This program is used by the automate script for setting a pipeline for a new repository\n");
    printf("You can run this program directly with arguments to set your desired variables\n");
    printf("\n");
    printf("Use the following commands for configuration\n");
    printf("\t-a  --apitoken        : API token for Jenkins script user (default default_apitoken) \n");
    printf("\t-b  --branch          : Branch to check on SCM (default master)\n");
    printf("\t-c  --credentialsid   : Configured credentials on Jenkins server to login to the repository (default GitUser)\n");
    printf("\t-f  --jenkinsfilename : Name of the Jenkinsfile on SCM (default Jenkinsfile)\n");
    printf("\t-g  --giturl          : Repository URL (default http://my-git-server.com/my-repo)\n");
    printf("\t-h  --help            : Show help page\n");
    printf("\t-i  --pipeline        : Name of the created pipeline (default default_pipeline)\n");
    printf("\t-j  --jenkins         : Jenkins URL (default http://my-jenkins-server.com/jenkins)\n");
    printf("\t-l  --list            : List current configuration\n");
    printf("\t-m  --mattermost      : Mattermost URL, set to disable if not needed (default http://my-mattermost-server.com/mattermost)\n");
    printf("\t-p  --pipe            : Enable/disable pipe with enable or disable string (default enable)\n");
    printf("\t-o  --mattermost_hook : Set the location of the mattermost hook on the mattermost url (default hooks/abcde12345...)\n");
    printf("\t-r  --periodictimer   : Build periodically, set to disable if not needed (format * * * * *, default disable)\n");
    printf("\t-t  --token           : Pipeline token for webhooks (default default_token)\n");

    return 0;
}

void prompt_user(char* question, char* key, int yes_no, struct tuple * tuple_list,int size)
{
        char buffer[500];
        int input;

        printf("%s: ",question);
        input = scanf("%[^\n]", buffer);
        getchar();

        if(yes_no){
                while((strcmp(buffer,"enable") != 0 ) && (strcmp(buffer,"disable") != 0 )){
                        printf("Please answer with enable or diable: ");
                        scanf("%s",buffer);
                }
        }
        if(input > 0)
        {
                printf("Setting new value: %s\n", buffer);
                set_variable(key, buffer,tuple_list,size);
        }
        else
        {
                printf("Using default value\n");
        }
}

void clean_up(struct tuple* tuple_list, int size)
{
       for(int i=0;i<size;i++){
              free(tuple_list[i].key);
              free(tuple_list[i].value);
       }
       free(tuple_list);
}

int run(int argc, char ** argv)
{
    int option_index=0;
    int option;
    int ret;
    int tuple_size = get_input_lines();

    char buf[500];

    struct tuple* tuple_list; //pointer to a list of tuple pointers

    //allocate memory for tuple_size tuple pointers
    tuple_list = malloc(sizeof(struct tuple)*tuple_size);
    if(tuple_list==NULL){printf("malloc failed\n");}

    load_variables(tuple_list);

    if(argc > 1 && strcmp(argv[1], "build") == 0)
    {
        generate_config_XML(tuple_list, tuple_size);
    }

    while((option = getopt_long(argc, argv, "a:i:m:o:j:r:g:f:c:b:t:p:lh", long_options, &option_index)) != -1)
    {
        switch (option) 
        {
            case 'm':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Mattermost URL, set to disable if not needed (default http://my-mattermost-server.com/mattermost)","MATTERMOST_URL",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("MATTERMOST_URL", optarg,tuple_list,tuple_size);
                break;
            case 'o':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Set the location of the mattermost hook on the mattermost url (default hooks/abcde12345...)", "MATTERMOST_HOOK",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("MATTERMOST_HOOK", optarg,tuple_list,tuple_size);
                break;
            case 'i':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Name of the created pipeline (default default_pipeline)","PIPELINE",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("PIPELINE", optarg,tuple_list,tuple_size);
                break;
            case 'a':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("API token for Jenkins script user (default default_apitoken)","PASS",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("PASS", optarg,tuple_list,tuple_size);
                break;
            case 'j':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Jenkins URL (default http://my-jenkins-server.com/jenkins)","JENKINS_URL",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("JENKINS_URL", optarg,tuple_list,tuple_size);
                break;
            case 'r':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Build periodically, set to disable if not needed (format * * * * *, default disable)","PERIODIC_TIMER",0,tuple_list,tuple_size); 
                    break;
                }
                set_variable("PERIODIC_TIMER", optarg,tuple_list,tuple_size);
                break;
            case 'g':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Repository URL (default http://my-git-server.com/my-repo)","GIT_URL",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("GIT_URL", optarg,tuple_list,tuple_size);
                break;
            case 'f':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Name of the Jenkinsfile on SCM (default Jenkinsfile)","JENKINSFILE_NAME",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("JENKINSFILE_NAME", optarg,tuple_list,tuple_size);
                break;
            case 'c':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Configured credentials on Jenkins server to login to the repository (default GitUser)","CREDENTIALS_ID",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("CREDENTIALS_ID", optarg,tuple_list,tuple_size);
                break;
            case 'b':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Branch to check on SCM (default master)","BRANCH",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("BRANCH", optarg,tuple_list,tuple_size);
                break;
            case 't':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Pipeline token for webhooks (default default_token)","TOKEN",0,tuple_list,tuple_size);
                    break;
                }
                set_variable("TOKEN", optarg,tuple_list,tuple_size);
                break;
            case 'p':
                if(strcmp(optarg, "w") == 0)
                {
                    prompt_user("Enable/disable pipe with enable or disable string (default enable)","PIPE",1,tuple_list,tuple_size);
                    break;
                }
                set_variable("PIPE", optarg,tuple_list,tuple_size);
                break;
            case 'l':
                list_variables(tuple_list,tuple_size);
                break;
            case 'h':
                get_help();
                break;
            case '?':
                break;
            default:
                printf("I don't know this one...\n");
                break;

        }
    }
    
    ret = write_file(tuple_list,tuple_size);
    if(ret == -1)
    {
        perror("Something wrong on writing to file");
        exit(-1);
    }
        generate_config_XML(tuple_list, tuple_size);
    
   snprintf(buf,sizeof(buf),"%s/%s",get_variable_value("MATTERMOST_URL",tuple_list,tuple_size),get_variable_value("MATTERMOST_HOOK",tuple_list,tuple_size));

        create_githook("./templates/pre-push.template","./output/pre-push",buf,get_variable_value("JENKINS_URL",tuple_list,tuple_size),get_variable_value("TOKEN",tuple_list,tuple_size));
    
    clean_up(tuple_list,tuple_size);
    return 0;

}
