#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "variables.h"

int run(int argc, char ** argv);
int get_help();

#endif