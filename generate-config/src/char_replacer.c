#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

const int __BUF_SIZE = 500;

/*
 * This function will replace the first nr_of_arg occurences of "special_char" in the src file
 * with the arguments from the arguments array.
 */
void replace_special_char(int src_fd, int dest_fd,char special_char, int nr_of_args, char* arguments[])
{
        int size=0; 
        int arg_count=0;
        char buf[__BUF_SIZE];

        //loop over all characters in the file 
        while((size=read(src_fd,buf,__BUF_SIZE))!=0){
                for(int i=0;i<size;i++){
                        if((buf[i] == special_char) && (arg_count < nr_of_args)){
                                write(dest_fd,arguments[arg_count],strlen(arguments[arg_count]));
                                //printf("%s",arguments[arg_count]);
                                arg_count++;
                        }else{
                                write(dest_fd,&buf[i],1);
                                //printf("%c",buf[i]);
                        }
                }
        }
}

/* Example programm to use the function
 *
int main(int argc, char* argv[])
{
        char* arguments[NR_OF_ARGS];

        //-----open files
        int template_fd = open(githook_template_file,O_RDONLY);
        int target_fd = open(githook_target_file, O_CREAT | O_TRUNC | O_RDWR,0644);
        
        ERROR_CHECK(template_fd,"open template file"); 
        ERROR_CHECK(target_fd,"open target file"); 
        
        //-----list of arguments
        arguments[MATTERMOST_URL] = "'https://vps439017.ovh.net/hooks/7qo94hjnoirguf4cfo6mngj3wy'";
        arguments[TOKEN]="'jenkins'"; 
       
        replace_special_char(template_fd,target_fd,'&',sizeof(arguments)/sizeof(arguments[0]),&arguments[0]); 

        //-----clean-up
        template_fd = close(template_fd);
        target_fd = close(target_fd);
        
        ERROR_CHECK(template_fd,"close githook");
        ERROR_CHECK(target_fd,"close githook");

        return 0;
}
*/
