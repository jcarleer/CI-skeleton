#ifndef __char_replacer_h
#define __char_replacer_h

void replace_special_char(int src_fd, int dest_fd,char special_char, int nr_of_args, char* arguments[]);

#endif /* __char_replacer_h*/
