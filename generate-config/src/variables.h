#ifndef VARIABLES_H
#define VARIABLES_H

#define TUPSIZE 11

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

struct tuple {
    char * key;
    char * value;
};

int get_input_lines();
int open_variables();
int close_variables(int fd);
char * read_variables(int fd);
char * get_line(char * buffer, char** saveptr);
char * get_word(char * input);
int load_variables(struct tuple* tuple_list);
int list_variables(struct tuple* tuples, int size);
int set_variable(char * key, char * value,struct tuple* tuple,int size);
int write_file(struct tuple* tuples,int size);
char * get_variable_value(char * key,struct tuple* tuple,int size);

#endif
