## generate_config

This folder contains everything you need to automatically set-up connections
between jenkins, kallithea and mattermost and to create a pipeline.First run the
mxml_installer.sh in the folder and than run the automate_setup.sh script in Wizard mode from within the generate_config folder
and you will be guided true the setup

## Jenkins file and Mattermost_post.sh

The Jenkinsfile and the mattermost_post.sh file are just templates to show what
you can do to send build results from jenkins to mattermost and send emails to
users when a build fails/succeeds : to enable emailing you should configure the
settings in Manage-jenkins -> configure system -> Extended E-mail Notification
