#!/bin/bash 
USER="\"Jenkins\""
MESSAGE="\"$1\""
IMAGE="\"https://www.cloudbees.com/sites/default/files/advisor.png\""
MATTERMOST_URL=$(cat ./generate-config/output/.variables | grep MATTERMOST_URL | cut -d " " -f 2)
MATTERMOST_HOOK=$(cat ./generate-config/output/.variables | grep MATTERMOST_HOOK | cut -d " " -f 2)

curl -i -X POST -H 'Content-Type: application/json' -d "{\"username\": $USER,\"text\": $MESSAGE,\"icon_url\": $IMAGE}" $MATTERMOST_URL/$MATTERMOST_HOOK
