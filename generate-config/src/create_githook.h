#ifndef __CREATE_GITHOOK_H
#define __CREATE_GITHOOK_H

int create_githook(char* githook_template_file,char* githook_target_file, char* mattermost_url, char* jenkins_url, char* token);

#endif /*__CREATE_GITHOOK_H */
