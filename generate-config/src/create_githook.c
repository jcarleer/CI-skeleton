#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "char_replacer.h"
#include "create_githook.h"

#define ERROR_CHECK(__err,__err_prefix) do{\
                                                if(__err==-1)\
                                                {\
                                                        perror(__err_prefix);\
                                                        exit(1);\
                                                }\
                                        }while(0)


#define MALLOC_CHECK(__RET) do{\
                                if(__RET==NULL)\
                                {\
                                        printf("MALLOC FAILED!\n");\
                                        exit(1);\
                                }\
                            }while(0)

#define NR_OF_ARGS 2
#define MATTERMOST_URL 0
#define JOB_TRIGGER_URL 1

const int MAX_BUF_SIZE = 500;

int create_githook(char* githook_template_file,char* githook_target_file, char* mattermost_url, char* jenkins_url, char* token)
{
        char* arguments[NR_OF_ARGS];
        char tmp_buf[MAX_BUF_SIZE];

        //-----open files
        int template_fd = open(githook_template_file,O_RDONLY);
        int target_fd = open(githook_target_file, O_CREAT | O_TRUNC | O_RDWR, 0744);
        
        ERROR_CHECK(template_fd,"open template file"); 
        ERROR_CHECK(target_fd,"open target file"); 
        
        //-----READ in the needed arguments
        
        //Mattermost incomming webhook URL
        arguments[MATTERMOST_URL] = malloc((strlen(mattermost_url)*sizeof(char))+1); //allocate memory for the input
        MALLOC_CHECK(arguments[MATTERMOST_URL]); 
        strncpy(arguments[MATTERMOST_URL],mattermost_url,strlen(mattermost_url)+1);

        //Jenkins job trigger url
        snprintf(tmp_buf,MAX_BUF_SIZE,"%sgeneric-webhook-trigger/invoke?token=%s",jenkins_url,token);
        arguments[JOB_TRIGGER_URL] = malloc((strlen(tmp_buf)*sizeof(char))+1); //allocate memory for the input
        MALLOC_CHECK(arguments[JOB_TRIGGER_URL]); 
        strncpy(arguments[JOB_TRIGGER_URL],tmp_buf,strlen(tmp_buf)+1);
       
        

        //-----replace the placeholders with the actual arguments 
        replace_special_char(template_fd,target_fd,'&',sizeof(arguments)/sizeof(arguments[0]),&arguments[0]); 

        //-----clean-up
        template_fd = close(template_fd);
        target_fd = close(target_fd);

        ERROR_CHECK(template_fd,"close githook");
        ERROR_CHECK(target_fd,"close githook");

        free(arguments[JOB_TRIGGER_URL]);
        free(arguments[MATTERMOST_URL]);

        return 0;
}
