#!/bin/bash

#STATIC VARIABLES
CONFIG="./output/config.xml"
PIPE_FOUND="302"


#--------------CONFIGURE ALL SETTINGS-------

echo "===CONFIGURATION==="

if [ "$1" == "WIZARD_CONFIG" ]; then
        make clean > /dev/null
        make > /dev/null
        echo "wizard mode selected"
        ./output/config_generator -gw -bw -fw -jw -iw -tw -cw -mw -ow -aw

elif [ "$1" == "MANUAL_CONFIG" ]; then
        echo "manual config selected"
        if [ -e ./output/.variables ]; then
               echo ".variables file exists, continuing..."
        else
               echo ".variables file does not exist, run with WIZARD_CONFIG or DEFAULT_CONFIG"
               exit 1;
        fi 


elif [ "$1" == "DEFAULT_CONFIG" ]; then
        make clean > /dev/null
        make > /dev/null
        echo "manual config selected"
        ./output/config_generator
else
        echo "please use one of the following arguments:"
        echo "WIZARD_CONFIG: to launch a configuration wizard"
        echo "MANUAL_CONFIG: to use the config in .variables (program should have run at least once before)"
        echo "DEFAULT_CONFIG: to use the configuration in .variables_default"
        exit 1
fi

#----------get configs from ./output/.variables-------------

JENKINS_URL=$(cat ./output/.variables | grep JENKINS_URL | cut -d " " -f 2)
PIPE_LINE=$(cat ./output/.variables | grep PIPELINE | cut -d " " -f 2)

USER=$(cat ./output/.variables | grep USER | cut -d " " -f 2)
APITOKEN=$(cat ./output/.variables | grep PASS | cut -d " " -f 2)

#-----------------GIT HOOKS----------------

echo "===CREATING GIT HOOK==="

#copy the pre-push file to the right location
cp ./output/pre-push ../.git/hooks/

echo "DONE"

#---------------JENKINS PIPELINE-----------

echo "===CREATING PIPELINE==="

#create the pipeline of needed
PIPE_STATUS=$(curl -s -o /dev/null -w "%{http_code}" -u $USER:$APITOKEN $JENKINS_URL/job/$PIPE_LINE)

if [ "$PIPE_STATUS" == "$PIPE_FOUND" ]; then
        echo "[info]pipeline already exists, no pipeline created"
else
        echo "[info]adding pipeline"
        CRUMB=$(curl -k -u $USER:$APITOKEN "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)" --location)
        curl -k -XPOST -u $USER:$APITOKEN "$JENKINS_URL/createItem?name=$PIPE_LINE" -H "$CRUMB" -H "Content-Type:text/xml" --data-binary @$CONFIG

fi

echo "DONE"
