#!/bin/bash

if [ "$(id -u)" != 0 ]; then
    echo "This script should be run as root to work";
    exit -1;
fi

sudo apt-get install -y gcc sudo make

mkdir mxml
tar -xf mxml-2.11.tar.gz -C mxml
cd mxml && \
./configure && \
make && \
sudo make install

sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf'
sudo ldconfig
ldconfig -p | grep local
