#include "xmlcreator.h"

int generate_config_XML(struct tuple * tuple_list, int size)
{
    int ret;
    char * variable;

    mxml_node_t * xml;
    mxml_node_t * flow;
    mxml_node_t * flowproperty;
    mxml_node_t * throttle;
    mxml_node_t * trigger;
    mxml_node_t * triggerproperty;
    mxml_node_t * triggervariable;
    mxml_node_t * scm;
    mxml_node_t * scmproperty;
    mxml_node_t * value;

    xml = mxmlNewXML("1.0");

    flow = mxmlNewElement(xml, "flow-definition");
    mxmlElementSetAttr(flow, "plugin", "workflow-job@2.12.2");
    flowproperty = mxmlNewElement(flow, "description");
    mxmlNewText(flowproperty, 0, "Please insert more info about your kallithea project");
    flowproperty = mxmlNewElement(flow, "keepDependencies");
    mxmlNewText(flowproperty, 0, "false");

    /* ---------- */
    /* PROPERTIES */
    /* ---------- */
    flowproperty = mxmlNewElement(flow, "properties");
    
    // --- THROTTLE BUILDS
    if((variable = get_variable_value("THROTTLE", tuple_list, size)) != NULL && strcmp(variable,"disabled"))
    {
        throttle = mxmlNewElement(flowproperty, "jenkins.branch.RateLimitBranchProperty_-JobPropertyImpl");
        mxmlElementSetAttr(throttle, "plugin", "branch-api@2.0.17");
        value = mxmlNewElement(throttle, "durationName");
        mxmlNewText(value, 0, "hour");
        value = mxmlNewElement(throttle, "count");
        mxmlNewText(value, 0, "10");
        value = mxmlNewElement(throttle, "userBoost");
        mxmlNewText(value, 0, "true");
    }
    // --- THROTTLE BUILDS

    // --- PIPELINE TRIGGER
    trigger = mxmlNewElement(flowproperty, "org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty");
    trigger = mxmlNewElement(trigger, "triggers");

    // --- PIPELINE TRIGGER AUTOMATED TRIGGER
    if((variable = get_variable_value("PERIODIC_TIMER", tuple_list, size)) == NULL)
    {
        perror("PERIODIC_TIMER does not exist in the default variables file");
        printf("%s\n", strerror(errno));
        return -1;
    }
    if(strcmp(variable,"disabled"))
    {
        triggerproperty = mxmlNewElement(trigger, "hudson.triggers.TimerTrigger");
        value = mxmlNewElement(triggerproperty, "spec");
        mxmlNewText(value, 0, "H 2 * * *");
    }
    // --- PIPELINE TRIGGER AUTOMATED TRIGGER

    // --- PIPELINE TRIGGER GENERIC WEBHOOK
    triggerproperty = mxmlNewElement(trigger, "org.jenkinsci.plugins.gwt.GenericTrigger");
    mxmlElementSetAttr(triggerproperty, "plugin", "generic-webhook-trigger@1.20");
    value = mxmlNewElement(triggerproperty, "spec");
    // --- PIPELINE TRIGGER GENERIC WEBHOOK GENERIC VARIABLE
    if((variable = get_variable_value("VARIABLE_GENERIC", tuple_list, size)) != NULL && strcmp(variable,"disabled"))
    {
        triggervariable = mxmlNewElement(triggerproperty, "genericVariables");
        triggervariable = mxmlNewElement(triggervariable, "org.jenkinsci.plugins.gwt.GenericVariable");
        value = mxmlNewElement(triggervariable, "expressionType");
        mxmlNewText(value, 0, "JSONPath");
        value = mxmlNewElement(triggervariable, "key");
        mxmlNewText(value, 0, "contentparam");
        value = mxmlNewElement(triggervariable, "value");
        value = mxmlNewElement(triggervariable, "regexpFilter");
        value = mxmlNewElement(triggerproperty, "regexpFilterText");
        value = mxmlNewElement(triggerproperty, "regexpFilterExpression");
    }
    // --- PIPELINE TRIGGER GENERIC WEBHOOK GENERIC REQUEST VARIABLE
    if((variable = get_variable_value("THROTTLE", tuple_list, size)) != NULL && strcmp(variable,"disabled"))
    {
        triggervariable = mxmlNewElement(triggerproperty, "genericRequestVariables");
        triggervariable = mxmlNewElement(triggervariable, "org.jenkinsci.plugins.gwt.GenericRequestVariable");
        value = mxmlNewElement(triggervariable, "key");
        mxmlNewText(value, 0, "requestparam");
        value = mxmlNewElement(triggervariable, "regexpFilter");
    }
    // --- PIPELINE TRIGGER GENERIC WEBHOOK GENERIC REQUEST VARIABLE
    // --- PIPELINE TRIGGER GENERIC WEBHOOK GENERIC HEADER VARIABLE
    triggervariable = mxmlNewElement(triggerproperty, "genericHeaderVariables");
    triggervariable = mxmlNewElement(triggervariable, "org.jenkinsci.plugins.gwt.GenericHeaderVariable");
    value = mxmlNewElement(triggervariable, "key");
    mxmlNewText(value, 0, "headerparam");
    value = mxmlNewElement(triggervariable, "regexpFilter");
    // --- PIPELINE TRIGGER GENERIC WEBHOOK GENERIC HEADER VARIABLE
    // --- PIPELINE TRIGGER GENERIC WEBHOOK
    // --- PIPELINE TRIGGER
    /* ---------- */

    /* ---------- */
    /* DEFINITION */
    /* ---------- */
    flowproperty = mxmlNewElement(flow, "definition");
    mxmlElementSetAttr(flowproperty, "class", "org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition");
    mxmlElementSetAttr(flowproperty, "plugin", "workflow-cps@2.42");

    // --- SCM
    scm = mxmlNewElement(flowproperty, "scm");
    mxmlElementSetAttr(scm, "class", "hudson.plugins.git.GitSCM");
    mxmlElementSetAttr(scm, "plugin", "git@3.6.4");
    value = mxmlNewElement(scm, "configVersion");
    mxmlNewText(value, 0, "2");

    // --- SCM USER REMOTE CONFIGS
    scmproperty = mxmlNewElement(scm, "userRemoteConfigs");
    scmproperty = mxmlNewElement(scmproperty, "hudson.plugins.git.UserRemoteConfig");
    value = mxmlNewElement(scmproperty, "url");
    mxmlNewText(value, 0, get_variable_value("GIT_URL", tuple_list, size));
    value = mxmlNewElement(scmproperty, "credentialsId");
    mxmlNewText(value, 0, get_variable_value("CREDENTIALS_ID", tuple_list, size));
    // --- SCM USER REMOTE CONFIGS

    // --- SCM BRANCHES
    scmproperty = mxmlNewElement(scm, "branches");
    scmproperty = mxmlNewElement(scmproperty, "hudson.plugins.git.BranchSpec");
    value = mxmlNewElement(scmproperty, "name");
    mxmlNewText(value, 0, get_variable_value("BRANCH", tuple_list, size));
    // --- SCM BRANCHES

    value = mxmlNewElement(scm, "doGenerateSubmoduleConfigurations");
    mxmlNewText(value, 0, "false");
    value = mxmlNewElement(scm, "submoduleCfg");
    mxmlElementSetAttr(value, "class", "list");
    value = mxmlNewElement(scm, "extensions");
    // --- SCM

    value = mxmlNewElement(flowproperty, "scriptPath");
    mxmlNewText(value, 0, get_variable_value("JENKINSFILE_NAME", tuple_list, size));
    value = mxmlNewElement(flowproperty, "lightweight");
    mxmlNewText(value, 0, "true");
    /* ---------- */

    flowproperty = mxmlNewElement(flow, "triggers");
    flowproperty = mxmlNewElement(flow, "authToken");
    mxmlNewText(flowproperty, 0, get_variable_value("TOKEN", tuple_list, size));
    flowproperty = mxmlNewElement(flow, "disabled");

    mxmlNewText(flowproperty, 0, get_variable_value("PIPE", tuple_list, size));

    ret = save_config_XML(xml);
    if(ret == -1)
    {
        perror("Could not save XML");
        return -1;
    }
        
    mxmlDelete(xml);

    return 0;
}

int save_config_XML(mxml_node_t * tree)
{
    FILE *fp;
    fp = fopen("./output/config.xml", "w");
    if(fp == NULL)
    {
        perror("Error opening config.xml\n");
        printf("%s\n", strerror(errno));
        return -1;
    }
    mxmlSaveFile(tree, fp, MXML_NO_CALLBACK);
    if(fclose(fp))
    {
        perror("Error closing config.xml\n");
        printf("%s\n", strerror(errno));
        return -1;
    }
    return 0;
}
