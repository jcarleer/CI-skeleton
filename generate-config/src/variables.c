#include "variables.h"

//struct tuple * tuples[TUPSIZE];

int get_input_lines(){
    int fd, amount;
    char * text;
    char * line;
    char * saveline;

    fd = open_variables();
    text = read_variables(fd);
    amount = 0;

    line = get_line(text,&saveline);
    while(line != NULL)
    {
        ++amount;
        line = get_line(NULL,&saveline);
    }
    close_variables(fd);

    free(text);

    return amount;
}

int open_variables()
{
    int fd;

    if((fd = open("./output/.variables", O_RDONLY, S_IRUSR)) == -1)
    {
        fd = open("./config/.variables_default", O_RDONLY, S_IRUSR);
    }
    
    if(fd == -1){
        perror("Something wrong on opening variables");
        exit(-1);
    }

    return fd;
}

int close_variables(int fd)
{
    if(fd == -1){
        perror("Something wrong on closing variables");
        exit(-1);
    }
    close(fd);
}

char * read_variables(int fd)
{
    struct stat buff;
    int buffer_size, ret;
    char *buffer;

    fstat(fd, &buff);
    buffer_size = buff.st_size;
    
    buffer = malloc(buffer_size+1);
    buffer[buffer_size] = '\0';
    ret = read(fd, buffer, buffer_size);

    if(ret == -1)
    {
        perror("Something wrong on reading variables");
        exit(-1);
    }

    return buffer;
}

char * get_line(char * input, char** original_input)
{
    char * line = strtok_r(input, "\n",original_input);
    return line;
}

char * get_word(char * input)
{
    char * word = strtok(input, " ");
    return word;
}

int load_variables(struct tuple* tuple_list)
{
    int fd;
    int line_size, word_size;
    char * buffer;
    char * line, *saveline;
    char * word, *saveword;
    
    fd = open_variables();
    buffer = read_variables(fd);

    line = get_line(buffer,&saveline);
    
    for(int i=0;i<get_input_lines();i++){
            line_size = strlen(line); 
            char* word = get_word(line);
            word_size = strlen(word);
            
            tuple_list[i].key = malloc((sizeof(char)*word_size)+1);
            tuple_list[i].value = malloc((sizeof(char)*(line_size-word_size))+1);

            strcpy(tuple_list[i].key,word);
            strcpy(tuple_list[i].value,&(line[strlen(word)+1]));

            line = get_line(NULL,&saveline);
    }
                
    free(buffer);
    close_variables(fd);
    return 0;
}

int list_variables(struct tuple* tuple_list, int size)
{
    for(int i=0; i<size; ++i)
    {
        printf("%s - %s \n", tuple_list[i].key, tuple_list[i].value);
    }    
    return 0;
}

int set_variable(char * key, char * value,struct tuple* tuple_list, int size)
{
    for(int i=0; i<size; ++i){
        if(strcmp(tuple_list[i].key, key) == 0)
        {
            if(tuple_list[i].value != NULL){free(tuple_list[i].value);}
            tuple_list[i].value = malloc(sizeof(char)*strlen(value)+1);
            strcpy(tuple_list[i].value, value);
        }
    }
}

int write_file(struct tuple* tuple_list, int size)
{
    int fd, i, ret;
    
    fd = open("./output/.variables", O_CREAT|O_TRUNC|O_RDWR, 0644);
    
    if(fd == -1){
        perror("Something wrong on opening variables");
        exit(-1);
    }

    for(i=0; i<size; ++i)
    {
        ret = write(fd, tuple_list[i].key, strlen(tuple_list[i].key));
        if(ret == -1){
            perror("Something wrong on writing variables to file");
            exit(-1);
        }
        ret = write(fd, " ", strlen(" "));
        if(ret == -1){
            perror("Something wrong on writing variables to file");
            exit(-1);
        }
        ret = write(fd, tuple_list[i].value, strlen(tuple_list[i].value));
        if(ret == -1){
            perror("Something wrong on writing variables to file");
            exit(-1);
        }
        ret = write(fd, "\n", strlen("\n"));
        if(ret == -1){
            perror("Something wrong on writing variables to file");
            exit(-1);
        }
    }

    close(fd);

    return 0;
}

char * get_variable_value(char * key, struct tuple * tuple_list, int size)
{
    for(int i=0; i<size; ++i)
    {
        if(strcmp(tuple_list[i].key, key) == 0)
        {
            return tuple_list[i].value;
        }
    }
    return NULL;
}
