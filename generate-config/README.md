## Install mxml
- grab the mxml stable version
- ./configure
- make
- sudo make install

# Make mxml linkable
The shared objects will be installed to /usr/local/lib. Add this to the dynamic linking configuration. 
```
sudo sh -c 'echo "/usr/local/lib" >> /etc/ld.so.conf'
sudo ldconfig
ldconfig -p | grep local
```

# Link the mxml library
Compile using GCC with
```
gcc -o output xxx.c xxx.c xxx.c -lmxml -pthread
